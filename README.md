# Software Implementation Small Airship #
## Bachelorarbeit: Entwicklung eines Kleinesluftschiffes mit Positionierungseinheit ##
=======

Author : **Muhammad Abiyyu Mufti Hanif**
Email : akang.mufti@gmail.com

### Summary ###
Arduino code for HSKA Small Airship, an indoor small ariship that is built for a bachelor thesis project

### Hardware Compatibility ###
This arduino code are used on Arduino Nano 33 IOT that based on SAMD21G microcontoller

### Arduino Pinout ###


### Aufgabenstellung ###

Kleinstluftschiffe haben hinsichtlich Ihrer Flugautonomie einen entscheidenden Vorteil im Vergleich zu Multicoptern. Während letztere Flugzeiten von nur 5-15min vorweisen, können bei richtiger Auslegung Luftschiffe um ein Vielfaches länger in der Luft bleiben. Motoren werden immer kurzzeitig verwendet, um ein bestimmtes Ziel anzufliegen, was zu einem deutlich niedrigeren Stromverbrauch führt.
Kleinstluftschiffe können Personen durch Gebäude führen oder auch Objekte von einem Raum zu einem anderen bringen, ohne eine Gefährdung durch starke Motoren oder hohen Geschwindigkeiten darzustellen. Die hier aufgebauten Luftschiffe sollen eine Untersuchung der Verwendbarkeit von Indoor-Drohnen bilden.
In das konstruierte Luftschiff soll ein aus einer vorhergegangenen Arbeit entwickeltes Positionierungssystem für Drohnen integriert werden, um die Lage des Fluggerätes zu anderen Flugteilnehmern und statischen Ankerpunkten bestimmen zu können.

**In einzelnen sind folgenden Punkte zu bearbeiten:**
* Komponentenrecherche, Dimensionierung, Auswahl und Test zum Aufbau eines Kleinstluftschiffes
* Zusammenbau eines steuerbaren Kleinstluftschiffes
* Einbau des Drohnenpositionierungssystems in das Luftschiff
* Positionierung des Luftschiffes im Raum zwischen zwei Ankerpunkten
* Option: Abfliegen eines definierten Weges 
* Option: Hindernisse detektieren und umfliegen
* Option: Eigenständiges fliegen von Raum zu Raum
* Test, Sammlung und Bewertung, sowie Präsentation, der Ergebnisse

### Content ### 

#### Program Ablauf ####

#### Additional SERCOM and I2C on Nano 33 IOT Configurations ####

Micro controller Atmel SAMD21G offerd a feature called I/O multiplexing with 6 SERCOM modules.
(details available page 21 in the data sheet Atmel SAM D21E / SAM D21G / SAM D21J), Each one of these modules can be used for I2C, SPI or Serial.

Some of the SERCOM are already used by the Arduino Nano 33 IoT:

* SERCOM2 for SPI NINA
* SERCOM3 for MOSI/MISO
* SERCOM4 for I2C bus
* SERCOM5 for Serial debugging (USB)

and that leaves only the Sercom 1 and Sercom 0. SERCOM 0 at Arduino Pins (6, 5) have alt pads (1,0), which means we can use it either for additional Serial or I2c. 
On the other hand SERCOM 1 at Arduino Pins (8, 13) have pads (2, 1), that can be used for additional Serial. 
HSKA Airship Board provide 2 plug in that connected to SERCOM 0 and SERCOM 1 (Note: SERCOM 1 Plug in instead of connected to 3.3V from arduino, its connected to 5V from the Voltage Regulator).

**To use addtional I2C from SERCOM 0**

    // Simply define I2C that attach to Sercom0 (6_SDA, 5_SCL)
    TwoWire SecondWire(&sercom0, 6, 5);
    // and then write the interrupt routing for SERCOM0
    void SERCOM0_Handler() {
	    SecondWire.onService();
    }    
    void setup(){
        // set the peripherals to PIO_SERCOM_ALT
        pinPeripheral(5, PIO_SERCOM_ALT);
        pinPeripheral(6, PIO_SERCOM_ALT);
        // and then you can use it normally like Wire from wire.h        
        SecondWire.begin();
    }
    
**To use addtional Serial from SERCOM 0**
(Note: either I2C or Serial cannot be both)

    // First declare a UART object with pins 5 and 6 for sercom 0
    Uart ISeria2(&sercom0, 5, 6, SERCOM_RX_PAD_1, UART_TX_PAD_0);
    // and then write the interrupt routing for SERCOM0
    void SERCOM0_Handler() {
	    ISeria2.IrqHandler();
    }
    void setup(){
        // lastly set the peripherals to PIO_SERCOM_ALT
        pinPeripheral(5, PIO_SERCOM_ALT);
        pinPeripheral(6, PIO_SERCOM_ALT);
        // and then you can use it normally like Serial from Arduino        
        ISeria2.begin(115200);
    }
    
**To use additonal Serial from SERCOM 1** 
(Note: with 5V)

    // Define UART ISerial1 that attach to Sercom1 (13_RX, 8_TX)
    Uart ISerial1(&sercom1, 13, 8, SERCOM_RX_PAD_1, UART_TX_PAD_2);
    void SERCOM1_Handler() {
	    ISerial1.IrqHandler();
    }
    void setup(){
        pinPeripheral(13, PIO_SERCOM);
        pinPeripheral(8, PIO_SERCOM);
        ISerial1.begin(TFMINI_BAUDRATE);
    }
    


