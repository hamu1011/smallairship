#include "FuzzyLogicAvoidance.h"

FuzzyInput *Error = new FuzzyInput(1);
FuzzySet *Danger = new FuzzySet(0, 0, 50, 100);
FuzzySet *Close = new FuzzySet(50, 100, 100, 200);
FuzzySet *Far = new FuzzySet(125, 200, 500, 500);

FuzzyInput *InputSpeed = new FuzzyInput(2);
FuzzySet *BrakeFast = new FuzzySet(1000, 1000, 1100, 1400);
FuzzySet *BrakeSlow = new FuzzySet(1300, 1375, 1375, 1475);
FuzzySet *Idle = new FuzzySet(1450, 1500, 1500, 1550);
FuzzySet *ThrustSlow = new FuzzySet(1525, 1625, 1625, 1700);
FuzzySet *ThrustFast = new FuzzySet(1600, 1900, 2000, 2000);

FuzzyOutput *BrakeSpeed = new FuzzyOutput(1);
FuzzySet *noBrake = new FuzzySet(0, 0, 0, 100);
FuzzySet *slowBrake = new FuzzySet(50, 150, 150, 300);
FuzzySet *fastBrake = new FuzzySet(200, 500, 500, 500);

void setupFuzzyInput()
{
	Error->addFuzzySet(Far);
	Error->addFuzzySet(Close);
	Error->addFuzzySet(Danger);
	FuzzyController->addFuzzyInput(Error);

	InputSpeed->addFuzzySet(BrakeFast);
	InputSpeed->addFuzzySet(BrakeSlow);
	InputSpeed->addFuzzySet(Idle);
	InputSpeed->addFuzzySet(ThrustSlow);
	InputSpeed->addFuzzySet(ThrustFast);
	FuzzyController->addFuzzyInput(InputSpeed);
}

void setupFuzzyOutput()
{
	BrakeSpeed->addFuzzySet(noBrake);
	BrakeSpeed->addFuzzySet(slowBrake);
	BrakeSpeed->addFuzzySet(fastBrake);
	FuzzyController->addFuzzyOutput(BrakeSpeed);
}

void setupFuzzyControl()
{

	setupFuzzyInput();
	setupFuzzyOutput();

	FuzzyRuleConsequent *then_SlowBrake = new FuzzyRuleConsequent();
	then_SlowBrake->addOutput(slowBrake);
	FuzzyRuleConsequent *then_FastBrake = new FuzzyRuleConsequent();
	then_FastBrake->addOutput(fastBrake);
	FuzzyRuleConsequent *then_noBrake = new FuzzyRuleConsequent();
	then_noBrake->addOutput(noBrake);

	FuzzyRuleAntecedent *if_Danger_and_ThrustFast = new FuzzyRuleAntecedent();
	if_Danger_and_ThrustFast->joinWithAND(Danger, ThrustFast);
	FuzzyRule *Rule01 = new FuzzyRule(1, if_Danger_and_ThrustFast, then_FastBrake);
	FuzzyController->addFuzzyRule(Rule01);

	FuzzyRuleAntecedent *if_Danger_and_ThrustSlow = new FuzzyRuleAntecedent();
	if_Danger_and_ThrustSlow->joinWithAND(Danger, ThrustSlow);
	FuzzyRule *Rule02 = new FuzzyRule(2, if_Danger_and_ThrustSlow, then_FastBrake);
	FuzzyController->addFuzzyRule(Rule02);

	FuzzyRuleAntecedent *if_Danger_and_Idle = new FuzzyRuleAntecedent();
	if_Danger_and_Idle->joinWithAND(Danger, Idle);
	FuzzyRule *Rule03 = new FuzzyRule(3, if_Danger_and_Idle, then_SlowBrake);
	FuzzyController->addFuzzyRule(Rule03);

	FuzzyRuleAntecedent *if_Close_and_ThrustFast = new FuzzyRuleAntecedent();
	if_Close_and_ThrustFast->joinWithAND(Close, ThrustFast);
	FuzzyRule *Rule04 = new FuzzyRule(4, if_Close_and_ThrustFast, then_FastBrake);
	FuzzyController->addFuzzyRule(Rule04);

	FuzzyRuleAntecedent *if_Close_and_ThrustSlow = new FuzzyRuleAntecedent();
	if_Close_and_ThrustSlow->joinWithAND(Close, ThrustSlow);
	FuzzyRule *Rule05 = new FuzzyRule(5, if_Close_and_ThrustSlow, then_SlowBrake);
	FuzzyController->addFuzzyRule(Rule05);

	FuzzyRuleAntecedent *if_Close_and_Idle = new FuzzyRuleAntecedent();
	if_Close_and_Idle->joinWithAND(Close, Idle);
	FuzzyRule *Rule06 = new FuzzyRule(6, if_Close_and_Idle, then_noBrake);
	FuzzyController->addFuzzyRule(Rule06);

	FuzzyRuleAntecedent *if_Far_and_ThrustFast = new FuzzyRuleAntecedent();
	if_Far_and_ThrustFast->joinWithAND(Far, ThrustFast);
	FuzzyRule *Rule07 = new FuzzyRule(7, if_Far_and_ThrustFast, then_SlowBrake);
	FuzzyController->addFuzzyRule(Rule07);

	FuzzyRuleAntecedent *if_Far_and_ThrustSlow = new FuzzyRuleAntecedent();
	if_Far_and_ThrustSlow->joinWithAND(Far, ThrustSlow);
	FuzzyRule *Rule08 = new FuzzyRule(8, if_Far_and_ThrustSlow, then_noBrake);
	FuzzyController->addFuzzyRule(Rule08);

	FuzzyRuleAntecedent *if_Far_and_Idle = new FuzzyRuleAntecedent();
	if_Far_and_Idle->joinWithAND(Far, Idle);
	FuzzyRule *Rule09 = new FuzzyRule(8, if_Far_and_Idle, then_noBrake);
	FuzzyController->addFuzzyRule(Rule09);
}
