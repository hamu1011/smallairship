// Driver_DRV8833.h

#ifndef _DRIVER_DRV8833_h
#define _DRIVER_DRV8833_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

// Pinout Arduino Nano IOT in Circuit 
#define M1A1 12
#define M1A2 11
#define M2B1 10
#define M2B2 9
#define M3A1 A2
#define M3A2 A3


/*
H_Bridge Logic

xIN1 and xIN2 input pins control the state of the xOUT1 and xOUT2

xIN1 | xIN2 | xOUT1 | xOUT2 | States | Function
0	 | 0	| OPEN	| OPEN	|		 | Coast (Outputs off)
PWM	 | 0	| PWM	| L		| Fast	 | Forward/Coast at spead PWM
0	 | PWM	| L		| PWM	| Fast	 | Reverse/Coast at speed PWM
1	 | PWM	| ~PWM	| L		| Slow	 | Forward/Break at spead 100-PWM
PWM	 | 1 	| L		| ~PWM	| Slow	 | Reverse/Break at speed 100-PWM
1	 | 1	| L		| L		|		 | Brake/Slow Decay

*/

class DRV8833 {
public:
	DRV8833(int pin1, int pin2, int minInput, int maxInput, int neutralWidth, boolean invert, boolean doublePWM);

	void begin();
	void setPWMOutputMax(int maxPWM);
	void drive(int controlValue, int rampTime = 1, boolean brake = false, boolean neutralBrake = false);

private:
	int _pin1;
	int _pin2;
	int _minInput;
	int _maxInput;
	int _minNeutral;
	int _maxNeutral;
	int _controlValue;
	int _controlValueRamp;
	int _maxPWM;
	int _rampTime;
	boolean _brake;
	boolean _neutralBrake;
	boolean _invert;
	boolean _doublePWM;
	unsigned long _previousMillis = 0;
	byte _state = 0;
};

#endif

