// 
// 
// 

#include "BuzzerTone.h"

//Mario main theme melody
int melodyWhenOK[] = {
  NOTE_E7, NOTE_E7, 0, NOTE_E7,
  0, NOTE_C7, NOTE_E7, 0,
  NOTE_G7, 0, 0,  0,
  NOTE_G6, 0, 0, 0,

  NOTE_C7, 0, 0, NOTE_G6,
  0, 0, NOTE_E6, 0,
  0, NOTE_A6, 0, NOTE_B6,
  0, NOTE_AS6, NOTE_A6, 0,
};

int tempoWhenOK[] = {
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,

	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
	12, 12, 12, 12,
};

int melodyWhenError1[] = {
	NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
	NOTE_AS3, NOTE_AS4, 0,
	0,
	NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
	NOTE_AS3, NOTE_AS4, 0,
	0
};

int tempoWhenError1[] = {
	12, 12, 12, 12,
	12, 12, 6,
	3,
	12, 12, 12, 12,
	12, 12, 6,
	3
};

int melodyWhenError2[] = {
	NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
	NOTE_DS3, NOTE_DS4, 0,
	0,
	NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
	NOTE_DS3, NOTE_DS4, 0,
	0
};

int tempoWhenError2[] = {
	12, 12, 12, 12,
	12, 12, 6,
	3,
	12, 12, 12, 12,
	12, 12, 6,
	3
};

int melodyOnStart[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int tempoOnStart[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

int melodyNokia[] = {
	  NOTE_E5, NOTE_D5, NOTE_FS4, NOTE_GS4,
	  NOTE_CS5, NOTE_B4, NOTE_D4, NOTE_E4,
	  NOTE_B4, NOTE_A4, NOTE_CS4, NOTE_E4,
	  NOTE_A4
};

int tempoNokia[] = {
	  8, 8, 4, 4,
	  8, 8, 4, 4,
	  8, 8, 4, 4,
	  2,
};

void Sing(int * melody, int * tempo, int size)
{
	for (int thisNote = 0; thisNote < size; thisNote++) {

		// to calculate the note duration, take one second
		// divided by the note type.
		//e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
		int noteDuration = 1000 / tempo[thisNote];

		buzz(melodyPin, melody[thisNote], noteDuration);

		// to distinguish the notes, set a minimum time between them.
		// the note's duration + 30% seems to work well:
		int pauseBetweenNotes = noteDuration * 1.30;
		delay(pauseBetweenNotes);

		// stop the tone playing:
		buzz(melodyPin, 0, noteDuration);

	}
}

void BatteryOn()
{
	Sing(melodyOnStart, tempoOnStart, sizeof(melodyOnStart) / sizeof(int));
}

void InitFinished()
{
	Sing(melodyWhenOK, tempoWhenOK, sizeof(melodyWhenOK) / sizeof(int));
}

void Error1Occured()
{
	Sing(melodyWhenError1, tempoWhenError1, sizeof(melodyWhenError1) / sizeof(int));
}

void Error2Occured()
{
	Sing(melodyWhenError2, tempoWhenError2, sizeof(melodyWhenError2) / sizeof(int));
}

void Error3Occured() {
	Sing(melodyNokia, tempoNokia, sizeof(melodyNokia) / sizeof(int));
}

void BuzzNoDelay(uint16_t onTime, uint16_t offTime) {
	static unsigned long previousTime = millis();
	static unsigned long currentTime;
	currentTime = millis();
	if (toneUP)
	{
		digitalWrite(melodyPin, HIGH);
		if (currentTime - previousTime >= onTime) {
			toneUP = false;
			previousTime = currentTime;
		}
	}
	else
	{
		digitalWrite(melodyPin, LOW);
		if (currentTime - previousTime >= offTime) {
			toneUP = true;
			previousTime = currentTime;
		}
	}
}

void Invalid() {
	BuzzNoDelay(100, 1000);
}

void NoIBUS()
{
	BuzzNoDelay(500, 1000);
}

void RCNeedRestart() {
	BuzzNoDelay(100, 200);
}

void toneOFF() {
	digitalWrite(melodyPin, LOW);
}

void buzz(int targetPin, long frequency, long length) {
	long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
	//// 1 second's worth of microseconds, divided by the frequency, then split in half since
	//// there are two phases to each cycle
	long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
	//// multiply frequency, which is really cycles per second, by the number of seconds to
	//// get the total number of cycles to produce
	for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
		digitalWrite(targetPin, HIGH); // write the buzzer pin high to push out the diaphram
		delayMicroseconds(delayValue); // wait for the calculated delay value
		digitalWrite(targetPin, LOW); // write the buzzer pin low to pull back the diaphram
		delayMicroseconds(delayValue); // wait again or the calculated delay value
	}
}
