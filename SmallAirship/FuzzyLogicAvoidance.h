 // FuzzyLogicAvoidance.h

#ifndef _FUZZYLOGICAVOIDANCE_h
#define _FUZZYLOGICAVOIDANCE_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#include <Fuzzy.h>

extern Fuzzy *FuzzyController;

void setupFuzzyInput();

void setupFuzzyOutput();

void setupFuzzyControl();
#endif

