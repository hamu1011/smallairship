// ControlManager.h

#ifndef _CONTROLMANAGER_h
#define _CONTROLMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "IBUSManager.h"
#include "Driver_DRV8833.h"
#include <PID_v1.h>
#include <FlashAsEEPROM.h>

// State Machine Swtich Control Function
enum MFlightMode
{
	IDLE,
	MANUALLY,
	AUTOMATIC1,
	AUTOMATIC2,
	AUTOMATIC3,
	AUTOMATIC4,
	INVALID
};
extern MFlightMode FLIGHTMODE;
void checkStateAfterIDLE();
void checkStateAfterMANUALLY();
void checkStateAfterAUTOMATIC1();
void checkStateAfterAUTOMATIC2();
void checkStateAfterAUTOMATIC3();
void checkStateAfterAUTOMATIC4();
void checkStateAfterINVALID();
// --------------------------------------


// used for Feedback Sensor 
struct FeedbackSensors
{
	uint32_t Elevation;
	uint32_t RangeToObstacle;
	float AccX;
	float AccY;
	float AccZ;
	float RotX;
	float RotY;
	float RotZ;
};
extern FeedbackSensors SensorsVal;
// --------------------------------------

// used for IBUS Control
extern IBUSManager IBUS;
bool channelInStartValue();
bool IBUSVALUEValid();
// --------------------------------------


// used for Motor Control
extern int speed1, speed2, speed3;
extern DRV8833 M1, M2, M3;
void assignSpeed(const unsigned int &_speed1, const unsigned int &_speed2, const unsigned int &_speed3);
void actuateMotors();
// --------------------------------------

// used for Braking System
extern float rangeToObstacle;
extern int brake;
// --------------------------------------

// used for PID Control
struct PIDConstants {
	float kp;
	float ki;
	float kd;
};
extern PIDConstants KPID;
extern double elevSetpoint, elevSpeed, elevation;
extern PID ElevControl;
// --------------------------------------


// used for Tunning PID 
void checkPIDConstantsFromMemory();
void serialTuning();
template <class T> int EEPROM_writeAnything(int ee, const T& value);
template <class T> int EEPROM_readAnything(int ee, T& value);
// --------------------------------------

// used for monitoring 
void debugSerially();
void debugRange();
void debugControlSignal();
void debugMotorSpeed();
void debugPID();
void debugBrakingSystem();
// --------------------------------------


#endif

