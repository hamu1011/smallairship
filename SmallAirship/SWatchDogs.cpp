// 
// 
// 

#include "SWatchDogs.h"

WatchDogCounter::WatchDogCounter()
{
	timeOut = 10;
	reset();
}

void WatchDogCounter::start() {
	previousMillis = millis();
}

void WatchDogCounter::tick() {
	currentMillis = millis();
	long deltaTime = currentMillis - previousMillis;
	if (deltaTime >= 1000) {
		previousMillis = currentMillis;
		seconds = seconds + deltaTime / 1000;;
		if (seconds >= 60) {
			seconds = seconds - 60;
			minutes = minutes + 1;
			if (minutes == 60) {
				minutes = 0;
				hours = hours + 1;
				if (hours == 24) {
					hours = 0;
				} // end hrs check
			} // end minutes check
		} // end seconds check
	} // end time check
}

void WatchDogCounter::reset() {
	currentMillis = 0;
	previousMillis = millis();
	seconds = 0;
	minutes = 0;
	hours = 0;
}

void WatchDogCounter::setTimeOut(const unsigned int & timeOut)
{
	this->timeOut = timeOut;
}

bool WatchDogCounter::isTimeOut()
{
	currentMillis = millis();
	long deltaTime = currentMillis - previousMillis;
	if (deltaTime >= timeOut) {
		previousMillis = currentMillis;
		return true;
	}
	else
	{
		return false;
	}
	
}

bool WatchDogCounter::timePassed(const unsigned int & timePassed)
{
	static long previous = millis();;
	static long current;
	current = millis();
	//long deltaTime = current - previous;
	if (current - previous > timePassed) {
		previous = current;
		return true;
	}
	else
	{
		return false;
	}
}

int WatchDogCounter::getSeconds() {
	return seconds;
}

int WatchDogCounter::getMinutes() {
	return minutes;
}

int WatchDogCounter::getHours() {
	return hours;
}
