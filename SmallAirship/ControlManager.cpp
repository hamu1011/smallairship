// 
// 
// 

#include "ControlManager.h"
#include "BuzzerTone.h"

static bool DRange, DControl, DSpeed, DPID, DBrake;

void ignoreSerialInput() {
	if (Serial && Serial.available())
	{
		Serial.flush();
	}
}

// State Machine Swtich Control Function ----------------------------------------------------//
void checkStateAfterIDLE()
{
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 2000)
	{
		if (channelInStartValue()) {
			FLIGHTMODE = MANUALLY;
		}
		else {
			FLIGHTMODE = INVALID;
		}
	}
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = AUTOMATIC3;
	}
}

void checkStateAfterMANUALLY()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = IDLE;
	}
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 2000) {
		FLIGHTMODE = AUTOMATIC1;
	}
}

void checkStateAfterAUTOMATIC1()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = IDLE;
	}
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 2000) {
		FLIGHTMODE = MANUALLY;
	}
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = AUTOMATIC3;
	}
	if (IBUS.getChannel(5) == 2000 && IBUS.getChannel(6) == 2000) {
		FLIGHTMODE = AUTOMATIC2;
	}
}

void checkStateAfterAUTOMATIC2()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) { FLIGHTMODE = IDLE; }
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 2000) { FLIGHTMODE = AUTOMATIC1; }
	if (IBUS.getChannel(5) == 2000 && IBUS.getChannel(6) == 1000) { FLIGHTMODE = AUTOMATIC4; }
}

void checkStateAfterAUTOMATIC3()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) { FLIGHTMODE = IDLE; }
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 2000) { FLIGHTMODE = AUTOMATIC1; }
	if (IBUS.getChannel(5) == 2000 && IBUS.getChannel(6) == 1000) { FLIGHTMODE = AUTOMATIC4; }
}

void checkStateAfterAUTOMATIC4()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = IDLE;
	}
	if (IBUS.getChannel(5) == 2000 && IBUS.getChannel(6) == 2000) {
		FLIGHTMODE = AUTOMATIC2;
	}
	if (IBUS.getChannel(5) == 1500 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = AUTOMATIC3;
	}
}

void checkStateAfterINVALID()
{
	ignoreSerialInput();
	if (IBUS.getChannel(5) == 1000 && IBUS.getChannel(6) == 1000) {
		FLIGHTMODE = IDLE;
		toneOFF();
	}
}
// -------------------------------------------------------------------------------------------//


// used for IBUS Control ---------------------------------------------------------------------//
inline bool InStartValue(uint16_t channels, uint16_t startvalue) {
	return (channels <= startvalue + 10) && (channels >= startvalue - 10);
}

bool channelInStartValue() {
	return InStartValue(IBUS.getChannel(1), 1500) && InStartValue(IBUS.getChannel(2), 1500) && InStartValue(IBUS.getChannel(3), 1000);
}

bool IBUSVALUEValid() {
	for (int i = 1; i <= 6; i++)
	{
		if (IBUS.getChannel(i) < 1000 || IBUS.getChannel(i) > 2000)
		{
			return false;
		}
	}
	return true;
}
// -------------------------------------------------------------------------------------------//


// used for Motor Control --------------------------------------------------------------------//
void assignSpeed(const unsigned int &_speed1, const unsigned int &_speed2, const unsigned int &_speed3)
{
	speed1 = _speed1;
	speed2 = _speed2;
	speed3 = _speed3;
}

void actuateMotors()
{
	M1.drive(speed1);
	M2.drive(speed2);
	M3.drive(map(speed3, 1000, 2000, 1500, 2000));
}
// -------------------------------------------------------------------------------------------//


// used for Tunning PID ----------------------------------------------------------------------//
inline void showKPIDInUse() {
	Serial.print(" KP: ");
	Serial.print(ElevControl.GetKp());
	Serial.print(" KI: ");
	Serial.print(ElevControl.GetKi());
	Serial.print(" KD: ");
	Serial.println(ElevControl.GetKd());
}

inline void checkKPIDVariables() {
	Serial.print(" KP: ");
	Serial.print(KPID.kp);
	Serial.print(" KI: ");
	Serial.print(KPID.ki);
	Serial.print(" KD: ");
	Serial.println(KPID.kd);
}

void checkPIDConstantsFromMemory()
{
	if (EEPROM.isValid())
	{
		Serial.print("Retrieving PID Constats from memory:");
		// Set PID Tunnings based on saved data in EEPROM
		EEPROM_readAnything(0, KPID);
		ElevControl.SetTunings(KPID.kp, KPID.ki, KPID.kd);
		showKPIDInUse();

	}
	else
	{
		KPID.kp = ElevControl.GetKp();
		KPID.ki = ElevControl.GetKi();
		KPID.kd = ElevControl.GetKd();
		Serial.println("There are no constants saved in memory");
	}
}

template <class T> int EEPROM_writeAnything(int indexEEPROM, const T& value)
{
	const byte* p = (const byte*)(const void*)&value;
	int i;
	for (i = 0; i < sizeof(value); i++) {
		EEPROM.write(indexEEPROM++, *p++);
	}
	EEPROM.commit();
	return i;
}

template <class T> int EEPROM_readAnything(int ee, T& value)
{
	byte* p = (byte*)(void*)&value;
	int i;
	for (i = 0; i < sizeof(value); i++) {
		*p++ = EEPROM.read(ee++);
	}
	return i;
}

void serialTuning()
{
	// COMMAND LIST
	/*
		- SET K(P|I|D) [VALUE] 
			Set the value of KPID Constants
		- CHECKVAL
			Check the PID value saved in KPID Constants variable
		- TUNE
			Tune the PID with KPID Constants
		- GETKPID
			Get KPID Constats that are currently in use in PID
		- SAVE
			Save the KPID Constans in flash memory
		- AUTOTUNE
			Tune KPID Constants Automatically --> Hasn't been implemented yet
	*/
	if (Serial) {
		while (Serial.available())
		{
			String command = Serial.readStringUntil(' ');
			String value = Serial.readStringUntil('\n');
			//String input = Serial.readStringUntil('\n');
			command.trim();
			command.toUpperCase();
			value.trim();
			value.toUpperCase();
			if (command == "SET") {
				String number = value;
				number.remove(0, 2);
				float toFloat = number.toFloat();
				switch (value[1]) {
				case 'P':
					KPID.kp = toFloat;
					break;
				case 'I':
					KPID.ki = toFloat;
					break;
				case 'D':
					KPID.kd = toFloat;
					break;
				default:
					Serial.println("System didn't recoginize your command");
					break;
				}
				Serial.print(value[0]);
				Serial.print(value[1]);
				Serial.print(": ");
				Serial.println(toFloat);
				checkKPIDVariables();
			}
			else if (command == "CHECKVAL") {
				Serial.println("Check KPID Constants saved in Variables: ");
				checkKPIDVariables();
			}
			else if (command == "TUNE") {
				if ((ElevControl.GetKp() != KPID.kp) || (ElevControl.GetKi() != KPID.ki) || (ElevControl.GetKd() != KPID.kd))
				{
					Serial.print("Tuning PID with new value: ");
					ElevControl.SetTunings(KPID.kp, KPID.ki, KPID.kd);
					showKPIDInUse();
				}
				else
				{
					Serial.println(" No Change in KPID Constants - no Tuning");
				}
			}
			else if (command == "GETKPID") {
				Serial.println("KPID Constatns that are currently used in PID Control ");
				showKPIDInUse();
			}
			else if (command == "SAVE") {
				Serial.print("Saving KPID Constants that used in PID Control to flash memory: ");
				showKPIDInUse();
				KPID.kp = ElevControl.GetKp();
				KPID.ki = ElevControl.GetKi();
				KPID.kd = ElevControl.GetKd();

				EEPROM_writeAnything(0, KPID);
			}
			else if (command == "AUTOTUNE") {
				Serial.print("Autotune hasn't been implemented yet: ");
				// TODO: Posibility of doing autotune
			}
			else if (command == "DEBUGPID") {
				DPID = !DPID;
			}
			else if (command == "DEBUGBRAKE") {
				DBrake = !DBrake;
			}
			else if (command == "DEBUGCONTROL") {
				DControl = !DControl;
			}
			else if (command == "DEBUGSPEED") {
				DSpeed = !DSpeed;
			}
			else if (command == "DEBUGRANGE") {
				DRange = !DRange;
			}
			else {
				Serial.println("System didn't recoginize your command");
			}
		}
	}
}

// used for monitoring ------------------------------------------------------------------------//
void debugSerially()
{
	if (DRange){
		debugRange();
	}
	if (DControl) {
		debugControlSignal();
	}
	if (DSpeed) {
		debugMotorSpeed();
	}
	if (DPID) {
		debugPID();
	}
	if (DBrake)
	{
		debugBrakingSystem();
	}
	if (DRange || DControl || DSpeed || DPID || DBrake) {
		Serial.println();
	}
}

void debugRange()
{
	Serial.print("Elev - ");	
	if (ElevationSensor.ranging_data.range_status)
	{
		Serial.print(ElevationSensor.rangeStatusToString(ElevationSensor.ranging_data.range_status));
	}
	Serial.print(" : ");
	Serial.print(elevation);
	Serial.print(" Range : ");
	if (Range1.ranging_data.range_status)
	{
		Serial.println(ElevationSensor.rangeStatusToString(Range1.ranging_data.range_status));
	}
	Serial.print(" : ");
	Serial.print(rangeToObstacle);
}

void debugControlSignal()
{
	Serial.print(" MODE: ");
	static String modename[] = { "IDL", "MNL", "AT1","AT2", "AT3", "AT4","INVALID" };
	Serial.print(modename[FLIGHTMODE]);
	for (int i = 1; i <= 6; i++)
	{
		Serial.print(" |");
		Serial.print(i);
		Serial.print(" ");
		Serial.print(IBUS.getChannel(i));
	}
}

void debugMotorSpeed()
{
	Serial.print(" |SP1: ");
	Serial.print(speed1);
	Serial.print(" |SP2: ");
	Serial.print(speed2);
	Serial.print(" |SP3: ");
	Serial.print(speed3);
}

void debugPID()
{
	Serial.print(" Elv: ");
	Serial.print(elevation);
	Serial.print(" Set: ");
	Serial.print(elevSetpoint);
	Serial.print(" Spd: ");
	Serial.print(elevSpeed);
}

void debugBrakingSystem()
{
	Serial.print(" Rng: ");
	Serial.print(rangeToObstacle);
	Serial.print(" SPD: ");
	Serial.print(IBUS.getChannel(2));
	Serial.print(" BRK: ");
	Serial.print(brake);
}


// -------------------------------------------------------------------------------------------//








