// SWatchDogs.h

#ifndef _SWATCHDOGS_h
#define _SWATCHDOGS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class WatchDogCounter {
public:
	WatchDogCounter();
	void start();
	void tick();
	void reset();
	void setTimeOut(const unsigned int &timeOut);
	bool isTimeOut();
	bool timePassed(const unsigned int &timePassed);
	int getSeconds();
	int getMinutes();
	int getHours();


private:
	long currentMillis, previousMillis;
	unsigned int seconds, minutes, hours, timeOut;
	
};

static WatchDogCounter WD;

#endif

