/*
 Name:		SmallAirship.ino
 Created:	1/20/2020 5:33:49 PM
 Author:	Muhammad Hanif
 Description: 
*/

/*

NANO| PIN OUT	| Remark		|
D12	| M1A1		|				|
D11	| M1A2		|				|
D10	| M1B1		|				|
D9	| M1B2		|				|
D8	| RX_TFMINI	|				|
D7	| -			|				|
D6	| SDA0/TX0	| Additional	|
D5	| SCL0/RX0	| Additional	|
D4	| Servo3	| No PWM		|
D3	| Servo2	|				|
D2	| Servo1	|				|
RX	| IBUS_TX	| flysky/drpsytm|
TX	| -			|				|
D13	| TX_TFMINI	|				|
A0	| -			|				|
A1	| -			|				|
A2	| M2A1		|				|
A3	| M2A2		|				|
A4	| SDA		|				|
A5	| SCL		|				|
A6	| -			|				|
A7	| -			|				|
*/

#include <FlashAsEEPROM.h>
#include <SimpleKalmanFilter.h>
#include <Fuzzy.h>
#include <PID_v1.h>
#include <Wire.h>
#include <wiring_private.h>
#include "ControlManager.h"
#include "SWatchDogs.h"
#include "FuzzyLogicAvoidance.h"
#include "Driver_DRV8833.h"
#include "IBUSManager.h"
#include "SA_VL53L1X_API.h"
#include "Arduino_LSM6DS3.h"
#include "BuzzerTone.h"

#define SerialTuning
//#define useLSM6DS3
#define useVL53L1XSensors
#define TwoRangeSensors

// Global Variables

// For Signal Control -------------------------------------------//
IBUSManager IBUS;
MFlightMode FLIGHTMODE = IDLE;
// --------------------------------------------------------------//

// For Motor Control --------------------------------------------//
DRV8833 M1(M1A1, M1A2, 1000, 2000, 50, false, true);
DRV8833 M2(M2B1, M2B2, 1000, 2000, 50, false, true);
DRV8833 M3(M3A1, M3A2, 1000, 2000, 50, false, true);
int speed1, speed2, speed3;
// --------------------------------------------------------------//

// For Sensor's Measurement -------------------------------------//
FeedbackSensors SensorsVal;
#ifdef useVL53L1XSensors
VL53L1X ElevationSensor;
SimpleKalmanFilter ElevationFilter(2, 5, 0.01);
#ifdef TwoRangeSensors
VL53L1X Range1;
SimpleKalmanFilter Range1Filter(2, 5, 0.01);
#endif // TwoRangeSensors
#endif // userRangeSensors
// --------------------------------------------------------------//

// For Elevation Control ----------------------------------------//
double elevSetpoint, elevSpeed, elevation;
PIDConstants KPID;
PID ElevControl(&elevation, &elevSpeed, &elevSetpoint, 4, 10, 100, DIRECT);
// --------------------------------------------------------------//

// For Braking System -------------------------------------------//
Fuzzy *FuzzyController = new Fuzzy();
int brake = 0;
float rangeToObstacle;
// --------------------------------------------------------------//

void setup() {
	pinMode(melodyPin, OUTPUT);
	BatteryOn();
	
	Serial.begin(115200);
	
	// Setup IBUS Manager -------------------------------------------//
	IBUS.begin(Serial1);
	// --------------------------------------------------------------//

	// Setup All Three Motor Drivers --------------------------------//
	M1.begin();
	M1.setPWMOutputMax(175);
	M2.begin();
	M2.setPWMOutputMax(175);
	M3.begin();
	M3.setPWMOutputMax(175);
	// --------------------------------------------------------------//

	// Setup IMU LSM6DS3 --------------------------------------------//
#ifdef useLSM6DS3
	setupIMU();
#endif // useLSM6DS3
	// --------------------------------------------------------------//

	// Setup Range Sensors ------------------------------------------//
#ifdef useVL53L1XSensors
	setupSercomInterface();
	setupRangeSensors();
#endif // useVL53L1XSensors

#ifdef TwoRangeSensors
	setupFuzzyControl();
#endif // useTwoRangeSensors
	// --------------------------------------------------------------//

	// Setup PID Controller for Elevation ---------------------------//
	ElevControl.SetMode(MANUAL);
	ElevControl.SetOutputLimits(1000, 2000);
	ElevControl.SetSampleTime(10);
	checkPIDConstantsFromMemory();
	// --------------------------------------------------------------//

	delay(5000);
	InitFinished();
	WD.start();
}

void loop() {

	// READ CONTROL SIGNAL FROM RECEIVER ----------------------------//
	IBUS.readControlInput();
	checkIBUSValidity();
	if (IBUS.connectionTimeOut()) {
		FLIGHTMODE = IDLE;
#ifdef SerialTuning
		serialTuning();
#endif // SerialTuning
	}
	
	// --------------------------------------------------------------//
	

	// READ SENSOR SIGNALS ------------------------------------------//
#ifdef useVL53L1XSensors
	readRangeSensors();
#endif // useVL53L1XSensors
	// --------------------------------------------------------------//
#ifdef useLSM6DS3
	readIMUValues();
#endif // useLSM6DS3
	// ---------------------------------

	// Flight Mode - Program as State Machine -----------------------//
	switch (FLIGHTMODE)
	{
	case IDLE:
		ElevControl.SetMode(MANUAL);
		assignSpeed(1500, 1500, 1000);
		checkStateAfterIDLE();
		break;

	case MANUALLY:
		ElevControl.SetMode(MANUAL);
		assignSpeed(IBUS.getChannel(1), IBUS.getChannel(2), IBUS.getChannel(3));
		checkStateAfterMANUALLY();
		break;

	case AUTOMATIC1:
		ElevControl.SetMode(AUTOMATIC);
		updatePID();
		assignSpeed(IBUS.getChannel(1), IBUS.getChannel(2), elevSpeed);
		checkStateAfterAUTOMATIC1();
		break;

	case AUTOMATIC2:
		ElevControl.SetMode(MANUAL);
		updateBraking();
		assignSpeed(IBUS.getChannel(1), IBUS.getChannel(2) - brake, IBUS.getChannel(3));
		checkStateAfterAUTOMATIC2();
		break;

	case AUTOMATIC3:
		ElevControl.SetMode(AUTOMATIC);
		updatePID();
		assignSpeed(IBUS.getChannel(1), IBUS.getChannel(2), elevSpeed);
		checkStateAfterAUTOMATIC3();
		break;

	case AUTOMATIC4:
		ElevControl.SetMode(AUTOMATIC);
		updatePID();
		updateBraking();
		assignSpeed(IBUS.getChannel(1), IBUS.getChannel(2)-brake, elevSpeed);
		checkStateAfterAUTOMATIC4();
		break;

	case INVALID:
		Invalid();
		assignSpeed(1500, 1500, 1000);
		checkStateAfterINVALID();
		break;

	default:
		break;
	}
	// --------------------------------------------------------------//

	// Actuate Motors -----------------------------------------------//
	actuateMotors();
	// --------------------------------------------------------------//

	// SERIAL DEBUGING ----------------------------------------------//
	debugSerially();
	// --------------------------------------------------------------//
}

void checkIBUSValidity() {
	if (!IBUSVALUEValid()) {
		Serial.print(" error ");
		//FLIGHTMODE = IDLE;
		//RCNeedRestart();
		//IBUS.resetChannels();
	}
	else
	{
		//toneOFF();
	}
}

void updateBraking() {
	// Actuatate Brake value 
	FuzzyController->setInput(1, rangeToObstacle);
	FuzzyController->setInput(2, IBUS.getChannel(2));
	FuzzyController->fuzzify();
	brake = FuzzyController->defuzzify(1);
}

void updatePID() {
	// Actuate PID
	elevSetpoint = 125.0; //mapDouble(IBUS.getChannel(3), 1000, 2000, 0.0, 300.0);
	ElevControl.Compute();
}

void setupSercomInterface() {
	pinMode(3, OUTPUT);
	pinMode(4, OUTPUT);
	digitalWrite(3, LOW); 
	digitalWrite(4, LOW);
	delay(500);
	Wire.begin();
	Wire.setClock(400000);
	Wire.beginTransmission(0x29);
}

void setupRangeSensors() {
#ifdef useVL53L1XSensors
	digitalWrite(4, HIGH);
#ifdef TwoRangeSensors
	while (!Range1.init(&Wire)) {
		Serial.println("Failed to detect and initialize sensor! ");
		//Error2Occured();
		Error3Occured();
	}
	delay(100);
	Range1.setAddress(0x33);
#endif // TwoRangeSensors
	delay(150);
	digitalWrite(3, HIGH);
	delay(150);
	while (!ElevationSensor.init(&Wire)) {
		Serial.println("Failed to detect and initialize sensor! ");
		Error1Occured();
	}
	delay(100);
	
	ElevationSensor.setDistanceMode(VL53L1X::Long);
	ElevationSensor.setMeasurementTimingBudget(50000);
	ElevationSensor.startContinuous(50);
	ElevationSensor.setTimeout(100);

#ifdef TwoRangeSensors
	Range1.setDistanceMode(VL53L1X::Long);
	Range1.setMeasurementTimingBudget(50000);
	Range1.startContinuous(50);
	Range1.setTimeout(100);
#endif // TwoRangeSensors
	delay(150);
#endif
}

void readRangeSensors() {
#ifdef useVL53L1XSensors
	if (ElevationSensor.dataReady())
	{
		SensorsVal.Elevation = ElevationSensor.read(false);
		if (ElevationSensor.ranging_data.range_status != 0){
			SensorsVal.Elevation = 2500;
		}
		
	}
	elevation = ElevationFilter.updateEstimate((float)SensorsVal.Elevation / 10.0);
#ifdef TwoRangeSensors
	if (Range1.dataReady())
	{
		SensorsVal.RangeToObstacle = Range1.read(false);
		if (Range1.ranging_data.range_status != 0){
			SensorsVal.RangeToObstacle = 3000;
		}
			
		
	}
	rangeToObstacle = Range1Filter.updateEstimate((float)SensorsVal.RangeToObstacle / 10.0);
#endif // TwoRangeSensors
#endif // useVL53L1XSensors
}

void setupIMU() {
#ifdef useLSM6DS3
	if (!IMU.begin()) {
		Serial.println("Failed to detect and initialize IMU");
		Error3Occured();
	}
#endif // useLSM6DS3
}

void readIMUValues() {
#ifdef useLSM6DS3
	if (IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
		IMU.readAcceleration(SensorsVal.AccX, SensorsVal.AccY, SensorsVal.AccZ);
		IMU.readGyroscope(SensorsVal.RotX, SensorsVal.RotY, SensorsVal.RotZ);
	}
#endif // useLSM6DS3
}

inline double mapDouble(double ibusVal, double in_min, double in_max, double out_min, double out_max) {
	return (ibusVal - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}