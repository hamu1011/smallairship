#include "IBUSManager.h"
#include "BuzzerTone.h"

/*--------------------------------------------------------------------------------*/

IBUSManager::IBUSManager()
{
	resetChannels();
	streamPtr = NULL;
	setAlarmTime(15);
}

IBUSManager::~IBUSManager()
{
	if (NULL != streamPtr)
	{
		delete streamPtr;
	}
}

void IBUSManager::begin(HardwareSerial & strPtr)
{
	MWD.start();
	strPtr.begin(115200);
	streamPtr = &strPtr;
}

bool IBUSManager::readControlInput()
{
	static bool newValue;
	MWD.tick();
	while (Serial1.available()) {
		Serial1.readBytes(Buffer, BUFFER_SIZE);
		MWD.reset();
		newValue = true;
	}
	if (newValue)
	{
		for (int count = 0; count < BUFFER_SIZE; count += 2) {
			/*Serial.print(" | ");
			Serial.print(Buffer[count]);
			Serial.print(" | ");
			Serial.print(Buffer[count+1]);*/
			if (Buffer[count] == 0x20 && Buffer[count + 1] == 0x40) {
				readChannelIndex = 0;
			}
			else {
				if (readChannelIndex < NUM_CHANNELS) {
					controlValue[readChannelIndex] = Buffer[count] + (Buffer[count + 1] << 8);
				}
				readChannelIndex++;
			}
		}
		newValue = false;
	}
	return false;
}

void IBUSManager::setAlarmTime(unsigned int timeInMin)
{
	timeout = timeInMin;
}

uint16_t IBUSManager::getChannel(unsigned char Index)
{
	if ((Index - 1) >= 0 && Index <= NUM_CHANNELS)
	{
		return controlValue[Index - 1];
	}

}

bool IBUSManager::getAllChannels(IBUS_CHANNELS & channels)
{
	channels.CH1 = controlValue[0];
	channels.CH2 = controlValue[1];
	channels.CH3 = controlValue[2];
	channels.CH4 = controlValue[3];
	channels.CH5 = controlValue[4];
	channels.CH6 = controlValue[5];
	channels.CH7 = controlValue[6];
	channels.CH8 = controlValue[7];
	channels.CH9 = controlValue[8];
	channels.CH10 = controlValue[9];
	channels.CH11 = controlValue[10];
	channels.CH12 = controlValue[11];
	channels.CH13 = controlValue[12];
	channels.CH14 = controlValue[13];
	return false;
}

bool IBUSManager::connectionTimeOut()
{
	if (MWD.getSeconds() >= 1) {
		resetChannels();
		if (MWD.getMinutes() >= timeout)
		{
			NoIBUS();
		}
		return true;
	}
	else
	{
		toneOFF();
		return false;
	}
}

bool IBUSManager::resetChannels()
{
	for (byte i = 0; i < NUM_CHANNELS; i++)
	{
		if (i == 0 || i == 4 || i == 5)
		{
			controlValue[i] = 1000;
		}
		controlValue[i] = 1500;
	}
	return false;
}


