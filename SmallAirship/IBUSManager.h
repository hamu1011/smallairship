// IBUSManager.h

#ifndef _IBUSMANAGER_h
#define _IBUSMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif


#include "HardwareSerial.h"
#include "SWatchDogs.h"

#define NUM_CHANNELS 14
#define STARTBYTE1 0x20
#define STARTBYTE2 0x40
#define BUFFER_SIZE 0x20

class IBUSManager
{
public:
	IBUSManager();
	~IBUSManager();
	void begin(HardwareSerial & strPtr);
	bool readControlInput();
	void setAlarmTime(unsigned int timeInSecond);
	uint16_t getChannel(unsigned char Index);
	bool getAllChannels(struct IBUS_CHANNELS &channels);
	bool connectionTimeOut();
	bool resetChannels();
private:
	HardwareSerial * streamPtr;
	uint16_t controlValue[NUM_CHANNELS];
	uint8_t Buffer[BUFFER_SIZE];
	uint8_t readChannelIndex;
	WatchDogCounter MWD;
	unsigned int timeout;
};


struct IBUS_CHANNELS
{
	uint16_t CH1;
	uint16_t CH2;
	uint16_t CH3;
	uint16_t CH4;
	uint16_t CH5;
	uint16_t CH6;
	uint16_t CH7;
	uint16_t CH8;
	uint16_t CH9;
	uint16_t CH10;
	uint16_t CH11;
	uint16_t CH12;
	uint16_t CH13;
	uint16_t CH14;
};

#endif

